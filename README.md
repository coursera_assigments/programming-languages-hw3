# Programming Languages (Coursera / University of Washington)

# Assignment 3

You will define several SML functions. Many will be very short because they will use other higher-order
functions. You may use functions in ML’s library; the problems point you toward the useful functions and
oftenrequirethat you use them. The sample solution is about 120 lines, including the provided code, but
not including the challenge problem. This assignment is probably more difficult than Homework 2 even
though (perhaps because) many of the problems have 1-line answers.

Downloadhw3provided.smlfrom the course website.

1. Write a functiononly_capitalsthat takes astring listand returns astring listthat has only
    the strings in the argument that start with an uppercase letter. Assume all strings have at least 1
    character. UseList.filter,Char.isUpper, andString.subto make a 1-2 line solution.
2. Write a functionlongest_string1that takes astring listand returns the longeststringin the
    list. If the list is empty, return"". In the case of a tie, return the string closest to the beginning of the
    list. Usefoldl,String.size, and no recursion (other than the implementation offoldlis recursive).
3. Write a functionlongest_string2that is exactly likelongest_string1except in the case of ties
    it returns the string closest to the end of the list. Your solution should be almost an exact copy of
    longest_string1. Still usefoldlandString.size.
4. Write functionslongest_string_helper,longest_string3, andlongest_string4such that:
    - longest_string3has the same behavior aslongest_string1andlongest_string4has the
       same behavior aslongest_string2.
    - longest_string_helperhas type(int * int -> bool) -> string list -> string
       (notice the currying). This function will look a lot likelongest_string1andlongest_string
       but is more general because it takes a function as an argument.
    - Iflongest_string_helperis passed a function that behaves like>(so it returnstrueexactly
       when its first argument is stricly greater than its second), then the function returned has the same
       behavior aslongest_string1.
    - longest_string3andlongest_string4are defined withval-bindings and partial applications
       oflongest_string_helper.
5. Write a functionlongest_capitalizedthat takes astring listand returns the longest string in
    the list that begins with an uppercase letter, or""if there are no such strings. Assume all strings
    have at least 1 character. Use aval-binding and the ML library’sooperator for composing functions.
    Resolve ties like in problem 2.
6. Write a functionrev_stringthat takes astringand returns thestringthat is the same characters in
    reverse order. Use ML’sooperator, the library functionrevfor reversing lists, and two library functions
    in theStringmodule. (Browse the module documentation to find the most useful functions.)

The next two problems involve writing functions over lists that will be useful in later problems.

7. Write a functionfirst_answerof type(’a -> ’b option) -> ’a list -> ’b(notice the 2 argu-
    ments are curried). The first argument should be applied to elements of the second argument in order
    until the first time it returnsSOME vfor somevand thenvis the result of the call tofirst_answer.
    If the first argument returnsNONEfor all list elements, thenfirst_answershould raise the exception
    NoAnswer. Hints: Sample solution is 5 lines and does nothing fancy.


8. Write a functionall_answersof type(’a -> ’b list option) -> ’a list -> ’b list option
    (notice the 2 arguments are curried). The first argument should be applied to elements of the second
    argument. If it returnsNONEfor any element, then the result forall_answersisNONE. Else the
    calls to the first argument will have producedSOME lst1,SOME lst2, ...SOME lstnand the result of
    all_answersisSOME lstwherelstislst1,lst2, ...,lstnappended together (order doesn’t matter).
    Hints: The sample solution is 8 lines. It uses a helper function with an accumulator and uses@. Note
    all_answers f []should evaluate toSOME [].

The remaining problems use these type definitions, which are inspired by the type definitions an ML imple-
mentation would use to implement pattern matching:

datatype pattern = Wildcard | Variable of string | UnitP | ConstP of int
| TupleP of pattern list | ConstructorP of string * pattern
datatype valu = Const of int | Unit | Tuple of valu list | Constructor of string * valu

Givenvaluvandpatternp, eitherpmatchesvor not. If it does, the match produces a list ofstring * valu
pairs; order in the list does not matter. The rules for matching should be unsurprising:

- Wildcardmatches everything and produces the empty list of bindings.
- Variable smatches any valuevand produces the one-element list holding(s,v).
- UnitPmatches onlyUnitand produces the empty list of bindings.
- ConstP 17matches onlyConst 17and produces the empty list of bindings (and similarly for other
    integers).
- TupleP psmatches a value of the formTuple vsifpsandvshave the same length and for alli, the
    ithelement ofpsmatches theithelement ofvs. The list of bindings produced is all the lists from the
    nested pattern matches appended together.
- ConstructorP(s1,p)matchesConstructor(s2,v)ifs1ands2are the same string (you can compare
    them with=) andpmatchesv. The list of bindings produced is the list from the nested pattern match.
    We call the stringss1ands2theconstructor name.
- Nothing else matches.
9. (This problem uses thepatterndatatype but is not really about pattern-matching.) A functionghas
been provided to you.

```
(a) Usegto define a functioncount_wildcardsthat takes a pattern and returns how manyWildcard
patterns it contains.
(b) Usegto define a functioncount_wild_and_variable_lengthsthat takes a pattern and returns
the number ofWildcardpatterns it contains plus the sum of the string lengths of all the variables
in the variable patterns it contains. (UseString.size. We care only about variable names; the
constructor names are not relevant.)
(c) Usegto define a functioncount_some_varthat takes a string and a pattern (as a pair) and
returns the number of times the string appears as a variable in the pattern. We care only about
variable names; the constructor names are not relevant.
```

10. Write a functioncheck_patthat takes a pattern and returns true if and only if all the variables
    appearing in the pattern are distinct from each other (i.e., use different strings). The constructor
    names are not relevant. Hints: The sample solution uses two helper functions. The first takes a
    pattern and returns a list of all the strings it uses for variables. Usingfoldlwith a function that uses
    @is useful in one case. The second takes a list of strings and decides if it has repeats.List.existsmay
    be useful. Sample solution is 15 lines. These are hints: We are not requiringfoldlandList.exists
    here, but they make it easier.
11. Write a functionmatchthat takes avalu * patternand returns a(string * valu) list option,
    namelyNONEif the pattern does not match andSOME lstwherelstis the list of bindings if it does.
    Note that if the value matches but the pattern has no patterns of the formVariable s, then the result
    isSOME []. Hints: Sample solution has one case expression with 7 branches. The branch for tuples
    usesall_answersandListPair.zip. Sample solution is 13 lines. Remember to look above for the
    rules for what patterns match what values, and what bindings they produce. These are hints: We are
    not requiringall_answersandListPair.ziphere, but they make it easier.
12. Write a functionfirst_matchthat takes a value and a list of patterns and returns a
    (string * valu) list option, namelyNONEif no pattern in the list matches orSOME lstwhere
    lstis the list of bindings for the first pattern in the list that matches. Usefirst_answerand a
    handle-expression. Hints: Sample solution is 3 lines.

(Challenge Problem)Write a functiontypecheck_patternsthat “type-checks” apattern list. Types
for our made-up pattern language are defined by:

datatype typ = Anything (* any type of value is okay *)
| UnitT (* type for Unit *)
| IntT (* type for integers *)
| TupleT of typ list (* tuple types *)
| Datatype of string (* some named datatype *)

typecheck_patternsshould have type((string * string * typ) list) * (pattern list) -> typ option.
The first argument contains elements that look like("foo","bar",IntT), which means constructorfoo
makes a value of typeDatatype "bar"given a value of typeIntT. Assume list elements all have different
first fields (the constructor name), but there are probably elements with the same second field (the datatype
name). Under the assumptions this list provides, you “type-check” thepattern listto see if there exists
sometyp(call itt) thatallthe patterns in the list can have. If so, returnSOME t, else returnNONE.

You must return the “most lenient” type that all the patterns can have. For example, given patterns
TupleP[Variable("x"),Variable("y")]andTupleP[Wildcard,Wildcard], returnTupleT[Anything,Anything]
even though they could both have typeTupleT[IntT,IntT]. As another example, if the only patterns
areTupleP[Wildcard,Wildcard]andTupleP[Wildcard,TupleP[Wildcard,Wildcard]], you must return
TupleT[Anything,TupleT[Anything,Anything]].

Type Summary: Evaluating a correct homework solution should generate these bindings, in addition to
the bindings for datatype and exception definitions:

val g = fn : (unit -> int) -> (string -> int) -> pattern -> int
val only_capitals = fn : string list -> string list
val longest_string1 = fn : string list -> string
val longest_string2 = fn : string list -> string


val longest_string_helper = fn : (int * int -> bool) -> string list -> string
val longest_string3 = fn : string list -> string
val longest_string4 = fn : string list -> string
val longest_capitalized = fn : string list -> string
val rev_string = fn : string -> string
val first_answer = fn : (’a -> ’b option) -> ’a list -> ’b
val all_answers = fn : (’a -> ’b list option) -> ’a list -> ’b list option
val count_wildcards = fn : pattern -> int
val count_wild_and_variable_lengths = fn : pattern -> int
val count_some_var = fn : string * pattern -> int
val check_pat = fn : pattern -> bool
val match = fn : valu * pattern -> (string * valu) list option
val first_match = fn : valu -> pattern list -> (string * valu) list option

Of course, generating these bindings does not guarantee that your solutions are correct.Test your functions:
Put your testing code in a second file. We will not grade the testing file, nor will you turn it in, but surely
you want to run your functions and record your test inputs in a file.

Assessment: We will automatically test your functions on a variety of inputs, including edge cases. We
will also ask peers to evaluate your code for simplicity, conciseness, elegance, and good formatting including
indentation and line breaks. Do not use SML’s mutable references or arrays.

Turn-in Instructions

First, follow the instructions on the course website to submit your solution file (not your testing file) for auto-
grading. Do not proceed to the peer-assessment submission until you receive a high-enough grade from the
auto-grader: Doing peer assessment requires instructions that include a sample solution, so these instructions
will be “locked” until you receive high-enough auto-grader score. Then submit your same solution file again
for peer assessment and follow the peer-assessment instructions.


