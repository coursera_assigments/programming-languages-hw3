(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer


(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)

fun only_capitals xs = 
    List.filter (fn x => Char.isUpper(String.sub(x,0))) xs

fun longest_string1 xs = List.foldl (fn (x, acc) => 
                                        if String.size x > String.size acc 
                                        then x else acc) "" xs

fun longest_string2 xs = List.foldl (fn (x, acc) => 
                                        if String.size x >= String.size acc 
                                        then x else acc) "" xs


fun longest_string_helper f xs =  List.foldl(fn (x, acc) => 
                                                if f (String.size x,String.size acc) 
                                                then x else acc) "" xs


val longest_string3 = longest_string_helper (fn (x,y) => x > y)

val longest_string4 = longest_string_helper (fn (x,y) => x >= y)

val longest_capitalized  = longest_string1 o only_capitals

fun rev_string str = (String.implode o List.rev o String.explode) str

fun first_answer f xs = case xs of 
                        [] => raise NoAnswer
                        |x::xs' => case f x of 
                                    SOME v => v
                                    | NONE => first_answer f xs'

fun all_answers f xs = 
                    let 
                        fun aux xs acc =
                            case xs of 
                                [] => SOME acc
                                |x::xs' => case f x of
                                    NONE => NONE
                                    | SOME n => aux xs' (n@acc)
                    in
                        aux xs []
                    end


datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end


val count_wildcards = g (fn () =>  1) (fn x => 0) 

val count_wild_and_variable_lengths = g (fn () =>  1) (fn x => String.size x)

fun count_some_var (str, p) = g (fn () =>  0) (fn x  => if x = str then 1 else 0) p 

fun check_pat p = 
                let 
                    fun convert_to_list p  =
	                    case p of
	                        Wildcard          => []
	                    | Variable x        => [x]
	                    | TupleP ps         => List.foldl (fn (p,acc) => (convert_to_list p) @ acc) [] ps
	                    | ConstructorP(_,p) => convert_to_list p
	                    | _                 => []
                    fun get_repeated xs = (List.foldl (fn (x, acc) => if x = acc then acc else x) "" xs)
                    fun is_repeat xs =  if get_repeated xs = "" then false  else true

                    val list = convert_to_list p
                in
                    is_repeat list
                end

fun match (v, p) = 
	case (v, p) of
		(_, Wildcard)	 		 					 => SOME []
	  | (_, Variable s)   		 					 => SOME [(s, v)]
	  | (Unit, UnitP)        		 				 => SOME []
	  | (Const c1, ConstP c)  	 		 			 => if c = c1 then SOME [] else NONE
	  | (Tuple vs, TupleP ps)	         			 => if length vs = length ps 
							  			       			then all_answers match (ListPair.zip (vs, ps)) 
											  			else NONE
	  | (Constructor (s2, v), ConstructorP (s1, p1)) => if s1 = s2 then match (v, p1) else NONE
	  | _ 					 						 => NONE

fun first_match v ps = 
	SOME (first_answer (fn p => match (v, p)) ps)
	handle NoAnswer => NONE